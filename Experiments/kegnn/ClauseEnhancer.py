import torch
from kegnn.boost_functions import *
import importlib


class ClauseEnhancer(torch.nn.Module):

    def __init__(self,
                 available_predicates: [str],
                 clause_string: str,
                 initial_clause_weight: float,
                 min_weight=0,
                 max_weight=500,
                 boost_function=GodelBoostConormApprox):
        """
        Initialize clause enhancer for a clause
        :param available_predicates: the list of all possible literals in a clause
        :param clause_string: a string representing a conjunction of literals. The format should be:
        clause_weight:clause
        :param initial_clause_weight: initialization of clause weight
        :param min_weight: minimum weight value for clipping
        :param max_weight: maximum weight value for clipping
        """
        super().__init__()
        boost_module = importlib.import_module("kegnn.boost_functions")
        _boost_function = getattr(boost_module, boost_function)

        weight_clause_split = clause_string.split(':')

        self.clause_string = weight_clause_split[1]
        weight_string = weight_clause_split[0]
        self.string = weight_clause_split[1].replace(
            ',', 'v').replace('(', '').replace(')', '')

        if weight_string == '_':
            initial_weight = initial_clause_weight
            fixed_weight = False
        else:
            initial_weight = float(weight_string)
            fixed_weight = True

        self.conorm_boost = _boost_function(initial_weight, fixed_weight, min_weight, max_weight)

        literals = self.clause_string.split(',')
        self.number_of_literals = len(literals)

        gather_literal_indices = []
        scatter_literal_indices = []
        signs = []

        for literal in literals:
            sign = 1
            if literal[0] == 'n':
                sign = -1
                literal = literal[1:]

            literal_index = available_predicates.index(literal)
            gather_literal_indices.append(literal_index)
            scatter_literal_indices.append([literal_index])
            signs.append(sign)

        self.register_buffer('signs', torch.tensor(signs, dtype=torch.float32))
        self.register_buffer('gather_literal_indices', torch.Tensor(gather_literal_indices).to(torch.long))
        self.register_buffer('scatter_literal_indices', torch.Tensor(scatter_literal_indices).to(torch.long))


    def forward(self, ground_atoms: torch.Tensor) -> (torch.Tensor, torch.Tensor):
        """
        Improve the satisfaction level of the clause.
        :param ground_atoms: the tensor containing the pre-activation values of the ground atoms
        :return: delta vector to be summed to the original pre-activation tensor to obtain an higher satisfaction of \
        the clause
        """
        selected_predicates = ground_atoms[..., self.gather_literal_indices]
        delta = self.conorm_boost(selected_predicates, self.signs)
        scattered_delta = torch.zeros_like(ground_atoms)
        scattered_delta[..., self.gather_literal_indices] = delta

        return delta, self.scatter_literal_indices
