import torch
from kegnn.KnowledgeEnhancer import KnowledgeEnhancer

from kegnn.boost_functions import GodelBoostConormApprox


class Ke(torch.nn.Module):

    def __init__(self, predicates: [str],
                 clauses: [str],
                 activation=lambda x: x,
                 initial_clause_weight=0.5,
                 save_training_data=False,
                 boost_function=GodelBoostConormApprox):
        """Initialize the knowledge base.
        :param predicates: a list of predicates names
        :param clauses: a list of constraints. Each constraint is a string on the form:
        clause_weight:clause
        :param activation: activation funcition
        :param initial_clause_weight: initialization of clause weight
        :save_training_data: if true, training data will be saved 
        """
        super().__init__()
        self.activation = activation
        self.knowledge_enhancer = KnowledgeEnhancer(
            predicates, clauses, initial_clause_weight, save_training_data, boost_function=boost_function)

    def reset_parameters(self):
        super().reset_parameters()

    def forward(self, inputs: torch.Tensor) -> (torch.Tensor, [torch.Tensor, torch.Tensor]):
        """
        Improve the satisfaction level of a set of clauses.
        :param ground_atoms: the tensor containing the pre-activation values of the ground atoms
        :return: final preactivations
        """
        deltas, deltas_list = self.knowledge_enhancer(inputs)
        return self.activation(inputs + deltas), deltas_list
